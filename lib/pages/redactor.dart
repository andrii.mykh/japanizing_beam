import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:japanizingbeam/classes/photo_parameters.dart';
import 'package:japanizingbeam/classes/styles/none_style.dart';
import 'package:japanizingbeam/services/constant/styles_list.dart';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:japanizingbeam/classes/styles/new_style.dart';
import 'package:japanizingbeam/services/function/button_text.dart';
import 'package:japanizingbeam/services/function/error_handler.dart';
import 'package:path_provider/path_provider.dart';


class Redactor extends StatefulWidget {
  @override
  _RedactorState createState() => _RedactorState();
}

class _RedactorState extends State<Redactor> {
  Map arg = {};
  String style;

  List<String> styles = stylesList;

  final myControllerForNumber = TextEditingController();

  @override
  void dispose() {
    try{
    // Clean up the controller when the widget is disposed.
    myControllerForNumber.dispose();
    }
    catch(e){
    print(errorHandler(e,'Dispose controller (redactor)'));
    }
    super.dispose();
  }

  ui.Image toSave;

  GlobalKey _globalKey = new GlobalKey();

  changePhoto() async{
    try{
    setState(() {
      arg['photo'].progressPercent = 1;
    });
    RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
    setState(() {
      arg['photo'].progressPercent = 5;
    });
    ui.Image image = await boundary.toImage(pixelRatio: 3.0);
    setState(() {
      arg['photo'].progressPercent = 10;
    });
    Size size = new Size(image.width*1.0, image.height*1.0);
    setState(() {
      arg['photo'].progressPercent = 20;
    });
    final ui.PictureRecorder recorder = ui.PictureRecorder();
    setState(() {
      arg['photo'].progressPercent = 30;
    });
    switch(arg['photo'].style){
      case 'none*':
        var color;
        if(arg['photo'].parametersList[0]=='text'){
          color = Colors.yellow[int.parse(arg['photo'].parametersList[1].toString())*100];
        }
        else if (arg['photo'].parametersList[0]=='int'){
          color = Colors.grey[int.parse(arg['photo'].parametersList[1].toString())*100];
        }
        setState(() {
          arg['photo'].progressPercent = 40;
        });
        NoneStyle(image: image, color: color).paint(Canvas(recorder), size);
        setState(() {
          arg['photo'].progressPercent = 50;
        });
        break;
      case 'new style interesting':
        var color;
        if(arg['photo'].parametersList[0]=='R'){color = Colors.red;}
        else if (arg['photo'].parametersList[0]=='G'){color = Colors.green;}
        else if(arg['photo'].parametersList[0]=='B'){color = Colors.blue;}
        setState(() {
          arg['photo'].progressPercent = 40;
        });
        NewStyle(image: image, color: color).paint(Canvas(recorder), size);
        setState(() {
          arg['photo'].progressPercent = 50;
        });
        break;
      case 'none':
        setState(() {
          arg['photo'].progressPercent = 0;
        });
        CupertinoAlertDialog(
          title: Text("Choose the style."),
      );         return true; break;
      default:
        setState(() {
          arg['photo'].progressPercent = 0;
        });
        CupertinoAlertDialog(
          title: Text("Some horrible error ocure. Check styles!"),
      ); return true; break;
    }
    final ui.Picture picture = recorder.endRecording();
    setState(() {
      arg['photo'].progressPercent = 60;
    });

    image = await picture.toImage(image.width, image.height);
    setState(() {
      toSave = image;
    });

    setState(() {
      arg['photo'].progressPercent = 70;
    });
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    setState(() {
      arg['photo'].progressPercent = 80;
    });
    //setState(() {
    //  this.
      Uint8List result = byteData.buffer.asUint8List();
    //});
    setState(() {
      arg['photo'].progressPercent = 90;
    });
    arg['photo'].image = Image.memory(result);
    setState(() {
      arg['photo'].progressPercent = 100;
    });
    }
    catch(e){
      print(errorHandler(e,'Change photo (redactor)'));
    }
  }

  @override
  Widget option(PhotoParameters photo){
    try{
      switch (photo.style){
        case 'none*':
          if(photo.parametersList.isEmpty||photo.parametersList.length!=2){
            photo.parametersList = new List(2);
          }
          myControllerForNumber.text = (photo.parametersList[1] is int)? photo.parametersList[1].toString():photo.parametersList[1]=1.toString();
          return Container(
          padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
            child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("First parameter: ", style: TextStyle(
                      color: Colors.grey[200],
                      fontSize: 18.0
                  ),),
                  Container(
                    color: Colors.grey[200],
                    child: DropdownButton<String>(
                      hint:  Text("${['text','int'].contains(photo.parametersList[0]) ? photo.parametersList[0]:photo.parametersList[0]='text'
                      }",
                      style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: 18.0
                      ),),
                      value: ['text','int'].contains(photo.parametersList[0]) ? photo.parametersList[0]:photo.parametersList[0]='text',
                      onChanged: (String Value) {
                        setState(() {
                          //must be spetial set
                          photo.parametersList[0] = Value;
                        });
                      },
                      items: ['text','int'].map((String style) {
                        return  DropdownMenuItem<String>(
                          value: style,
                          child: Text(
                            style,
                            style:  TextStyle(
                                color: Colors.grey[600],
                                fontSize: 18.0
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
              Row(
              children: <Widget>[
                  Container(
                    width: 200,
                    child: TextField(
                    controller: myControllerForNumber,
                    onChanged: (text)=>{photo.parametersList[1]=int.parse(text)<1 ? 1 : (int.parse(text)>9 ? 9 : int.parse(text))},
                    cursorColor: Colors.grey[200],
                    style: TextStyle(
                        color: Colors.grey[200],
                        fontSize: 18.0
                    ),
                    decoration: InputDecoration(
                      border: OutlineInputBorder( borderSide: BorderSide(
                        color: Colors.grey[200],
                        width: 2.0,
                      ),),
                      labelText: 'Second parameter',
                      labelStyle: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0
                      ),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly
                    ],),
                  ),
              ],
            ),
          ],
        ),);
        case 'new style interesting':
          if(photo.parametersList.isEmpty||photo.parametersList.length!=1){
            photo.parametersList = new List(1);
          }
          return Container(
            padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
            child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("First parameter: ", style: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0
                      ),),
                      Container(
                        color: Colors.grey[200],
                        child: DropdownButton<String>(
                          hint:  Text("${['R','G','B'].contains(photo.parametersList[0]) ? photo.parametersList[0]:photo.parametersList[0]='R'}", style: TextStyle(
                              color: Colors.grey[600],
                              fontSize: 18.0
                          ),),
                          value: ['R','G','B'].contains(photo.parametersList[0]) ? photo.parametersList[0]:photo.parametersList[0]='R',
                          onChanged: (String Value) {
                            setState(() {
                              //must be spetial set
                              photo.parametersList[0] = Value;
                            });
                          },
                          items: ['R','G','B'].map((String style) {
                            return  DropdownMenuItem<String>(
                              value: style,
                              child: Text(
                                style,
                                style:  TextStyle(
                                    color: Colors.grey[600],
                                    fontSize: 18.0
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ],
                  ),
                ]
            )
        );
        default: photo.parametersList = []; return Container();
      }
    }
    catch(e){
      print(errorHandler(e,'Redactor options'));
    }
  }

  savePhoto() async {
    try{
    ByteData dataSave = await this.toSave.toByteData(format: ui.ImageByteFormat.png);

    Directory appDocDirectory = await getExternalStorageDirectory();

    print(appDocDirectory.path);
    //   /storage/emulated/0
    Directory extDocDirectory = await getExternalStorageDirectory();

    DateTime now = DateTime.now();
    String formattedDate = now.toIso8601String();
    String total_path;

    final directory = await new Directory(extDocDirectory.path+'/'+'JapanizingBeam').create(recursive: true);

    total_path = directory.path;

    final file = File(total_path+'/JapanixingBeam$formattedDate.png');

    print("file://${file.path}");
    file.writeAsBytesSync(dataSave.buffer.asInt8List());
    }
    catch(e){
      print(errorHandler(e,'Save photo (redactor)'));
    }
  }

  @override
  Widget build(BuildContext context) {

    arg = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[800],
          actions: <Widget>[
            Container(
              color: Colors.grey[200],
              margin: EdgeInsets.fromLTRB(0,10,10,10),
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: DropdownButton<String>(
                hint:  Text("${arg['photo'].style}", style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 18.0
                ),),
                value: style,
                onChanged: (String Value) {
                  setState(() {
                    //must be spetial set
                    arg['photo'].style = Value;
                  });
                },
                items: styles.map((String style) {
                  return  DropdownMenuItem<String>(
                    value: style,
                    child: Text(
                      style,
                      style:  TextStyle(
                          color: Colors.grey[600],
                          fontSize: 18.0
                      ),
                    ),
                  );
                }).toList(),
              )
            )
          ],
        ),
        backgroundColor: Colors.grey[500],
        body: Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Visibility(
                visible: arg['photo'].style != 'none',
                child:
                  option(arg['photo']),
              ),
              Container(
                child: RepaintBoundary(
                  key: _globalKey,
                  child: arg['photo'].image,
                ),
              ),
              Column(children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(2, 0, 0, 2),
                  width: 156.0,
                  height: 86.0,
                  child: RaisedButton(
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: buttonText(arg['photo'].progressPercent,600,600,600),
                    onPressed: ()=>{
                      if(arg['photo'].progressPercent <= 0)
                        {
                          changePhoto()
                        }
                      else if(arg['photo'].progressPercent >= 100)
                        {
                            savePhoto(),
                            setState(()=>{arg['photo'].inList = false}),
                            Navigator.pushNamed(context,'/home',)
                        }
                    },
                  ),
                ),
                Container(
                  decoration: ShapeDecoration(
                    shape: OutlineInputBorder( borderSide: BorderSide(
                      color: Colors.grey[900],
                      width: 0.5,),
                    ),
                  ),
                  margin: EdgeInsets.fromLTRB(0, 2, 0, 0),
                  height: 16.0,
                  child: LinearProgressIndicator(
                    value: arg['photo'].progressPercent/100,
                    backgroundColor: Colors.grey[200],
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.grey[700]),
                  ),
                ),
              ],
              ),
            ]
        )
      )
    );
  }

}
