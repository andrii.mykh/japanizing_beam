import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:japanizingbeam/classes/photo_parameters.dart';
import 'package:japanizingbeam/services/function/error_handler.dart';
import 'package:japanizingbeam/services/list_item.dart';

import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  void stateServiceInPlatform() async{
    try{
    if(Platform.isAndroid){
      var methodChannel = MethodChannel("com.example.japanizingbeam");
      String data = await methodChannel.invokeMethod("startService");
      debugPrint(data);
    }
    }
    catch(e){
      print(errorHandler(e.toString(),'Android background'));
    }
  }

  final FirebaseMessaging _messaging = FirebaseMessaging();


  @override
  void initState() {
    super.initState();
    stateServiceInPlatform();
    try {
      _messaging.getToken().then((token) {
        print(token);//information for testing
      });
    }
    catch(e){
      print(errorHandler(e.toString(),'Firebase messaging about testing token'));
    }
  }

  List<ListPhotoItem> list = new List<ListPhotoItem>();

  _openGalary(BuildContext context)async{
    try {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);

      var photo = new PhotoParameters(inList: true,
          style: 'none',
          image: Image.file(imageFile),
          parametersList: new List(),
          progressPercent: 0);
      ListPhotoItem item = new ListPhotoItem(photo: photo);

      list.add(item);

      Navigator
          .of(context)
          .pop;
      Navigator.pushNamed(context, '/redactor', arguments: {'photo': photo});
    }
    catch(e){
      print(errorHandler(e.toString(),'Take photo from gallery'));
    }
  }

  _openCamera(BuildContext context)async{
    try{
    var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    var photo = new PhotoParameters(inList: true, style: 'none', image: Image.file(imageFile),
        parametersList: new List(), progressPercent: 0);
    ListPhotoItem item = new ListPhotoItem(photo: photo);

    list.add(item);

    Navigator.of(context).pop;
    Navigator.pushNamed(context,'/redactor', arguments: {'photo': photo});
    }
    catch(e){
      print(errorHandler(e.toString(),'Take photo from camera'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[800],
        title: Center(
          child: Text(
              "Japanizing Beam!",
              style: TextStyle(
                color: Colors.white,
                fontSize: 28.0
              ),
            ),
        ),
        leading: IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            Navigator.pushNamed(context,'/settings');
          },
        ),
        actions: [
        IconButton(
          icon: Icon(Icons.format_list_bulleted),
          onPressed: () {
            Navigator.pushNamed(context,'/photo_list', arguments: {'list': list});
          }
        ),
        ],
      ),
      backgroundColor: Colors.grey[500],
      body: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            FlatButton(
              child: Column(
                  children: <Widget>[
                    Icon(Icons.folder,size: 250,),
                    Text(
                      "Galery",
                      style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 32.0
                    ),
                  ),
                ],
              ),
              onPressed: () => _openGalary(context),
            ),
            SizedBox(height: 50.0,),
            FlatButton(
              child: Column(
                children: <Widget>[
                  Icon(Icons.camera_alt,size: 250,),
                  Text(
                    "New photo",
                    style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 32.0
                    ),
                  ),
                ],
              ),
              onPressed: () => _openCamera(context),
            ),
          ],
        ),
      )
    );
 }
}
