import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:japanizingbeam/services/date_picker.dart';
import 'package:japanizingbeam/services/for_authorization.dart';
import 'package:japanizingbeam/services/function/error_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {

  final NameController = TextEditingController();
  final SurnameController = TextEditingController();

  @override
  void dispose() {
    try{
    // Clean up the controller when the widget is disposed.
    NameController.dispose();
    SurnameController.dispose();
    }
    catch(e){
      print(errorHandler(e,'Dispose controller (settings)'));
    }
    super.dispose();
  }

  bool triggerValue, agree;
  String name, surname, birth, email, country;

  bool _isVisibleS = false;
  bool _isVisibleA = false;
  bool _isVisibleI = false;
  bool _isVisibleB = false;

  @override
  void initState() {
    super.initState();
    try{
    getValues();
    }
    catch(e){
      print(errorHandler(e.toString(),'Get values after it endes'));
    }
  }

  getValues() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      setState(() {
        triggerValue = prefs.getBool('trigger') ?? true;
        name = prefs.getString('name') ?? '';
        surname = prefs.getString('surname') ?? '';
        birth = prefs.getString('birth') ?? 'Birth date';
        email = prefs.getString('email') ?? '';
        country = prefs.getString('country');
        agree = prefs.getBool('agree') ?? false;
      });
    }
    catch(e){
      print(errorHandler(e.toString(),'Get user values'));
    }
  }

  save(String what) async{
    try {
      final prefs = await SharedPreferences.getInstance();
      switch (what) {
        case 'settings':
          prefs.setBool('trigger', this.triggerValue);
          break;
        case 'account':
          prefs.setString('name', this.name);
          prefs.setString('surname', this.surname);
          prefs.setString('birth', this.birth);
          prefs.setString('email', this.email);
          prefs.setString('country', this.country);
          prefs.setBool('agree', this.agree);
          break;
        default:
          print('Impossible action');
          break;
      }
    }
    catch(e){
      print(errorHandler(e.toString(),'Save user values'));
    }
  }

  @override
  Widget build(BuildContext context) {
    NameController.text = this.name;
    SurnameController.text = this.surname;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[800],
        title: Text(
          "Settings",
          style: TextStyle(
              color: Colors.white,
              fontSize: 28.0
          ),
        ),
      ),
      backgroundColor: Colors.grey[500],
      body: Column(
        children: <Widget>[
          SizedBox(height: 10.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                width: 400.0,
                child: RaisedButton(
                  onPressed: (){ setState(() {_isVisibleS = !_isVisibleS;}); },
                  textColor: Colors.grey[700],
                  color: Colors.grey[200],
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(width: 330.0, child: Text("Settings", style: TextStyle(fontSize: 32.0),),),
                      Icon(Icons.arrow_drop_down, color:Colors.grey[700], size: 50,),
                    ],
                  )
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0,0.0,5.0,0.0),
            child: Visibility(
              visible: _isVisibleS,
              child: Container(
                padding: const EdgeInsets.all(5.0),
                color: Colors.grey[600],
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Checkbox(
                          activeColor: Colors.grey[200],
                          checkColor: Colors.grey[600],
                          value: triggerValue,
                          onChanged: (bool value) {
                            setState(() {
                              triggerValue = value;
                              /*
                              /*
                                It is impossible now
                                because in this demonstrating version
                                authorization haven`t been complited
                              */

                              var Firestore;
                              Firestore.instance
                                  .collection('users')
                                  .document(loggedInUser.uid)
                                  .updateData({
                                'adminNotifications': value,
                              });*/
                            });
                          },),
                        SizedBox(width: 10.0,),
                        Text('Take notifications?', style: TextStyle(
                            color: Colors.grey[200],
                            fontSize: 18.0
                        ),)
                      ],
                    ),
                    SizedBox(height: 5.0,),
                    RaisedButton(
                        onPressed: (){
                          save('settings');
                          setState(() {_isVisibleS = !_isVisibleS;});
                          },
                        textColor: Colors.grey[700],
                        color: Colors.grey[200],
                        padding: EdgeInsets.fromLTRB(150, 0, 150, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("Save", style: TextStyle(fontSize: 18.0),)
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                width: 400.0,
                child: RaisedButton(
                    onPressed: (){setState(() {_isVisibleA= !_isVisibleA;}); },
                    textColor: Colors.grey[700],
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: 330.0, child: Text("Account", style: TextStyle(fontSize: 32.0),),),
                        Icon(Icons.arrow_drop_down, color:Colors.grey[700], size: 50,),
                      ],
                    )
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0,0.0,5.0,0.0),
            child: Visibility(
              visible: _isVisibleA,
              child: Container(
                padding: const EdgeInsets.all(5.0),
                color: Colors.grey[600],
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: NameController,
                      onChanged: (text)=>{name = text},
                      style: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder( borderSide: BorderSide(
                          color: Colors.grey[200],
                          width: 2.0,
                        ),),
                        labelText: 'Name',
                        labelStyle: TextStyle(
                            color: Colors.grey[200],
                            fontSize: 18.0
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    TextField(
                      controller: SurnameController,
                      onChanged: (text)=>{surname = text},
                      style: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder( borderSide: BorderSide(
                          color: Colors.grey[200],
                          width: 2.0,
                        ),),
                        labelText: 'Surname',
                        labelStyle: TextStyle(
                            color: Colors.grey[200],
                            fontSize: 18.0
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    DateTimePicker(
                      date: birth,
                      onChange: (date){
                      setState(() {
                        birth = date;
                      });
                    },),
                    SizedBox(height: 5.0,),
                    TextFormField(
                      style: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0
                      ),
                      decoration: InputDecoration(labelText: 'Email', labelStyle: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0
                      ),
                        border: OutlineInputBorder( borderSide: BorderSide(
                          color: Colors.grey[200],
                          width: 2.0,
                        ),),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator:  validateEmail,
                      onChanged: (email){this.email = email;},
                      initialValue: email,
                      ),
                    SizedBox(height: 5.0,),
                    Container(
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      height: 60.0,
                      width: 400.0,
                      decoration: ShapeDecoration(
                        shape: OutlineInputBorder( borderSide: BorderSide(
                          color: Colors.grey[700],
                          width: 2.0,
                        ),),
                      ),
                      child: Theme(
                        data: ThemeData(canvasColor: Colors.grey[700]),
                        child: DropdownButton<String>(
                          hint:  Text("Country", style: TextStyle(
                          color: Colors.grey[200],
                              fontSize: 18.0
                          ),),
                          style: TextStyle(
                              color: Colors.grey[200],
                              fontSize: 18.0
                          ),
                          value: country,
                          onChanged: (String Value) {
                            setState(() {
                              country = Value;
                            });
                          },
                          items: countries.map((String country) {
                            return  DropdownMenuItem<String>(
                              value: country,
                              child: Text(
                                country,
                                style:  TextStyle(
                                    color: Colors.grey[200],
                                    fontSize: 18.0
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    Container(
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      height: 60.0,
                      width: 400.0,
                      decoration: ShapeDecoration(
                        shape: OutlineInputBorder( borderSide: BorderSide(
                          color: Colors.grey[700],
                          width: 2.0,
                        ),),
                      ),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                            activeColor: Colors.grey[200],
                            checkColor: Colors.grey[600],
                            value: agree,
                            onChanged: (bool value) {
                            setState(() {
                                agree = value;
                              });
                            },),
                          SizedBox(width: 10.0,),
                          Text('Agree with license.', style: TextStyle(
                          color: Colors.grey[200],
                              fontSize: 18.0
                          ),)
                        ],
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    RaisedButton(
                        onPressed: (){if(this.agree) setState(() {
                          save('account');
                          _isVisibleA = !_isVisibleA;
                        }); },
                        textColor: Colors.grey[700],
                        color: Colors.grey[200],
                        padding: EdgeInsets.fromLTRB(150, 0, 150, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("Save", style: TextStyle(fontSize: 18.0),)
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                width: 400.0,
                child: RaisedButton(
                    onPressed: (){ setState(() { _isVisibleI = !_isVisibleI; }); },
                    textColor: Colors.grey[700],
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: 330.0, child: Text("Instruction", style: TextStyle(fontSize: 32.0),),),
                        Icon(Icons.arrow_drop_down, color:Colors.grey[700], size: 50,),
                      ],
                    )
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0,0.0,5.0,0.0),
            child: Visibility(
              visible: _isVisibleI,
              child: Container(
                padding: const EdgeInsets.all(5.0),
                color: Colors.grey[600],
                child: Column(
                  children: <Widget>[
                    Text('How to use app. So, it may take a short instruction about UX/UI.',style: TextStyle(
                        color: Colors.grey[200],
                        fontSize: 18.0
                    ),),
                    SizedBox(height: 5.0,),
                    RaisedButton(
                        onPressed: (){ setState(() {_isVisibleI = !_isVisibleI;}); },
                        textColor: Colors.grey[700],
                        color: Colors.grey[200],
                        padding: EdgeInsets.fromLTRB(150, 0, 150, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("OK", style: TextStyle(fontSize: 18.0),)
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                width: 400.0,
                child: RaisedButton(
                    onPressed: (){ setState(() { _isVisibleB = !_isVisibleB; }); },
                    textColor: Colors.grey[700],
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: 330.0, child: Text("About", style: TextStyle(fontSize: 32.0),),),
                        Icon(Icons.arrow_drop_down, color:Colors.grey[700], size: 50,),
                      ],
                    )
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0,0.0,5.0,0.0),
            child: Visibility(
              visible: _isVisibleB,
              child: Container(
                padding: const EdgeInsets.all(5.0),
                color: Colors.grey[600],
                child: Column(
                  children: <Widget>[
                    Text('About rules. About me. A lot of information about all copyrights. All styles of japanize anime directors and companies.',style: TextStyle(
                        color: Colors.grey[200],
                        fontSize: 18.0
                    ),),
                    SizedBox(height: 5.0,),
                    RaisedButton(
                        onPressed: (){ setState(() {_isVisibleB = !_isVisibleB;}); },
                        textColor: Colors.grey[700],
                        color: Colors.grey[200],
                        padding: EdgeInsets.fromLTRB(150, 0, 150, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("OK", style: TextStyle(fontSize: 18.0),)
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 5.0),
          SizedBox(height: 15.0, child: Text('Japanizing Beam v.0.2.5',style: TextStyle(color: Colors.white, fontSize: 12.0),),),
          SizedBox(height: 15.0, child: Text('Andrii Mykhailiv',style: TextStyle(color: Colors.white, fontSize: 12.0),),),
        ],
      ),
    );
  }
}



