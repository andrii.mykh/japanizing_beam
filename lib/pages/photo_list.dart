import 'package:flutter/material.dart';

class PhotoList extends StatefulWidget {
  @override
  _PhotoListState createState() => _PhotoListState();
}

class _PhotoListState extends State<PhotoList> {

  Map arg = {};

  @override
  Widget build(BuildContext context) {

    arg = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[800],
        title: Text(
          "Photos",
          style: TextStyle(
              color: Colors.white,
              fontSize: 28.0
          ),
        ),
      ),
      backgroundColor: Colors.grey[500],
      body: Center(child: Column( children: (arg['list'] == null ? [] : arg['list'])))
    );
  }
}
