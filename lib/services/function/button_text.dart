import 'package:flutter/material.dart';

import 'error_handler.dart';

Text buttonText (int procent, int firstColor, int secondColor, int thirdColor)
{
  try{
    if (procent <= 0) return Text('Start',style: TextStyle(color: Colors.grey[firstColor], fontSize: 36.0),);
    if (procent >= 100) return Text('Save',style: TextStyle(color: Colors.grey[secondColor], fontSize: 36.0),);
    return Text('Wait',style: TextStyle(color: Colors.grey[thirdColor], fontSize: 36.0),);
  }
  catch(e){
    print(errorHandler(e.toString(),'Button text'));
  }
}