import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<String> userInfo() async{
  final prefs = await SharedPreferences.getInstance();

  String  name = prefs.getString('name')??'none';
  String  surname = prefs.getString('surname')??'none';
  String birth = prefs.getString('birth')??'none';
  String email = prefs.getString('email')??'none';
  String  country = prefs.getString('country')??'none';

  return "User information.\n"+
         "Name: "+name+" Surname: "+surname+
         "\nCountry: "+country+" Birth date: "+birth+
         "\nE-mail:"+email;
}

Future<String> errorHandler(String text, String subject) async {
  final Email email = Email(
    body: "Ocure some mistake.\nDo You want to sent message to developer?\n\n"+
          (await userInfo())+"\n\nError information: \n"+text,
    subject: subject,
    recipients: ["andrii.mykhailiv.pz.2017@lpnu.ua"],
    isHTML: false,
  );

  await FlutterEmailSender.send(email);

  return text;
}