import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:japanizingbeam/classes/photo_parameters.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart';
import 'package:japanizingbeam/classes/styles/new_style.dart';
import 'package:japanizingbeam/classes/styles/none_style.dart';
import 'package:path_provider/path_provider.dart';

import 'function/button_text.dart';
import 'function/error_handler.dart';

class ListPhotoItem extends StatefulWidget {
  final PhotoParameters photo;
  ListPhotoItem({
    PhotoParameters photo
  }): this.photo = photo;
  @override
  _ListPhotoItemState createState() => _ListPhotoItemState(photo);
}

class _ListPhotoItemState extends State<ListPhotoItem> {

  _ListPhotoItemState(this.photo);
  final PhotoParameters photo;

  String parameters (var list) {
    try{
    if(list == null) return '';
    list = list.toList();
    String result = ' (';
    for (var item in list)
      result+= (item.toString() + ', ');
    result = result.substring(0, result.length - 2);
    return result == "" ? result : result+')';
    }
    catch(e){
      print(errorHandler(e.toString(),'Show parameters in list'));
    }
  }

  ui.Image toSave;

  GlobalKey _globalKey = new GlobalKey();

  void changePhoto() async{
    try{
      setState(() {
        photo.progressPercent = 1;
      });
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
      setState(() {
        photo.progressPercent = 5;
      });
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      setState(() {
        photo.progressPercent = 10;
      });
      Size size = new Size(image.width*1.0, image.height*1.0);
      setState(() {
        photo.progressPercent = 20;
      });
      final ui.PictureRecorder recorder = ui.PictureRecorder();
      setState(() {
        photo.progressPercent = 30;
      });
      switch(photo.style){
        case 'none*':
          var color;
          if(photo.parametersList[0]=='text'){
            color = Colors.yellow[int.parse(photo.parametersList[1].toString())*100];
          }
          else if (photo.parametersList[0]=='int'){
            color = Colors.grey[int.parse(photo.parametersList[1].toString())*100];
          }
          setState((){photo.progressPercent = 40;});
          NoneStyle(image: image, color: color).paint(Canvas(recorder), size);
          setState((){photo.progressPercent = 50;});
          break;
        case 'new style interesting':
          var color;//make try catch here
          if(photo.parametersList[0]=='R'){color = Colors.red;}
          else if (photo.parametersList[0]=='G'){color = Colors.green;}
          else if(photo.parametersList[0]=='B'){color = Colors.blue;}
          setState(() {
            photo.progressPercent = 40;
          });
          NewStyle(image: image, color: color).paint(Canvas(recorder), size);
          setState(() {
            photo.progressPercent = 50;
          });
          break;
        case 'none': CupertinoAlertDialog(
          title: Text("Choose the style."),
        ); break;
        default: CupertinoAlertDialog(
          title: Text("Some horrible error ocure. Check styles!"),
        ); break;
      }
      final ui.Picture picture = recorder.endRecording();
      setState(() {
        photo.progressPercent = 60;
      });
      image = await picture.toImage(image.width, image.height);
      setState(() {
        toSave = image;
      });
      setState(() {
        photo.progressPercent = 70;
      });
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      setState(() {
        photo.progressPercent = 80;
      });
      Uint8List pngBytes = byteData.buffer.asUint8List();
      setState(() {
        photo.progressPercent = 90;
      });
      photo.image = Image.memory(pngBytes);
      setState(() {
        photo.progressPercent = 100;
      });
    }
    catch(e){
    print(errorHandler(e.toString(),'Change photo (list)'));
    }
  }

  savePhoto() async {
    try{
    ByteData dataSave = await this.toSave.toByteData(format: ui.ImageByteFormat.png);

    Directory appDocDirectory = await getExternalStorageDirectory();
    print(appDocDirectory.path);
    //   /storage/emulated/0
    Directory extDocDirectory = await getExternalStorageDirectory();

    DateTime now = DateTime.now();
    String formattedDate = now.toIso8601String();
    String total_path;

    final directory = await new Directory(extDocDirectory.path+'/'+'JapanizingBeam').create(recursive: true);

    total_path = directory.path;

    final file = File(total_path+'/JapanixingBeam$formattedDate.png');

    print("file://${file.path}");
    file.writeAsBytesSync(dataSave.buffer.asInt8List());
    }
    catch(e){
    print(errorHandler(e.toString(),'Save photo (list)'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return photo.inList ? Container(
      width: 400.0,
      margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
      decoration: ShapeDecoration(
          shape: OutlineInputBorder( borderSide: BorderSide(
            color: Colors.grey[700],
            width: 2.0,),
          ),
        ),
      //color: Colors.grey[200],
      child: RaisedButton(
        onPressed: ()=>{
          //if(photo.progressPercent<=0||photo.progressPercent>=100)
            Navigator.pushNamed(context,'/redactor', arguments: {'photo': photo})
        },
        color: Colors.grey[400],
        child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          decoration: ShapeDecoration(
                            shape: OutlineInputBorder( borderSide: BorderSide(
                              color: Colors.grey[300],
                              width: 1.0,),
                            ),
                          ),
                        height: 70.0,
                        width: 200.0,
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: RepaintBoundary(
                                key: _globalKey,
                                child: photo.image,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 16.0,),
                      Text(
                        photo.style+parameters(photo.parametersList),
                        style: TextStyle(color: Colors.grey[700], fontSize: 18.0),
                      )
                    ],
                  ),
                  Column(children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 116,),
                          IconButton(
                            icon: Icon(Icons.indeterminate_check_box),
                            iconSize: 30.0,
                            onPressed: ()=>{
                              setState(()=>{photo.inList = false})
                            },
                          )
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(2, 0, 0, 2),
                        width: 156.0,
                        height: 86.0,
                        child: RaisedButton(
                          color: Colors.grey[700],
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: buttonText(photo.progressPercent, 200, 200, 600),
                          onPressed: ()=>{
                            if(photo.progressPercent <= 0)
                              {
                                changePhoto()
                              }
                            else if(photo.progressPercent >= 100)
                              {
                                savePhoto(),
                                setState(()=>{photo.inList = false})
                              }
                          },
                        ),
                      )
                    ],
                  ),
                ],
              ),
            Container(
                decoration: ShapeDecoration(
                  shape: OutlineInputBorder( borderSide: BorderSide(
                    color: Colors.grey[900],
                    width: 0.5,),
                  ),
                ),
                margin: EdgeInsets.fromLTRB(0, 2, 0, 0),
                height: 16.0,
                child: LinearProgressIndicator(
                  value: photo.progressPercent/100,
                  backgroundColor: Colors.grey[200],
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.grey[700]),
                ),
              ),
              SizedBox(height: 5.0,)
          ],
        ),
      ),
    ) : Container();
  }
}

/*
/*test data*/

PhotoParameters Test_Photo = new PhotoParameters(style: 'none*', image: Image(image: NetworkImage('https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg'),),
parametersList: ['text', 12], progressPercent: 20);*/
