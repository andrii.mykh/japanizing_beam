import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class DateTimePicker extends StatefulWidget {
  final ValueSetter<String> onChange;
  final String date;
  DateTimePicker({
    this.onChange, this.date
  });
  @override
  _DateTimePickerState createState() => _DateTimePickerState(date);
}

class _DateTimePickerState extends State<DateTimePicker> {

  _DateTimePickerState(this.date);
  String date;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
        shape: OutlineInputBorder( borderSide: BorderSide(
          color: Colors.grey[700],
          width: 2.0,
        ),),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 4.0,
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime(2022, 12, 31), onConfirm: (_date) {
                    print('confirm $_date');
                    setState(() {date = '${_date.year} - ${_date.month} - ${_date.day}';});
                    widget.onChange(this.date);
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
            },
            child: Container(
              //alignment: Alignment.center,
              height: 60.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      " $date",
                      style: TextStyle(
                          color: Colors.grey[200],
                          fontSize: 18.0),
                    ),
                  ),
                  Text(
                    "  Change",
                    style: TextStyle(
                        color: Colors.grey[200],
                        fontSize: 18.0),
                  ),
                ],
              ),
            ),
            color: Colors.grey[600],
          )
        ],
      ),
    );
  }
}