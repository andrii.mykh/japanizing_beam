import 'package:flutter/material.dart';
import 'package:japanizingbeam/pages/home.dart';
import 'package:japanizingbeam/pages/photo_list.dart';
import 'package:japanizingbeam/pages/redactor.dart';
import 'package:japanizingbeam/pages/settings.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/home', //Warning: When using initialRoute, don’t define a home property.
  routes: {
    '/home': (context) => Home(),
    '/photo_list': (context) => PhotoList(),
    '/redactor': (context) => Redactor(),
    '/settings': (context) => Settings(),
  },
));