import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:japanizingbeam/services/function/error_handler.dart';

class NoneStyle extends CustomPainter {


  NoneStyle({
    this.image,
    this.color
  });
  ui.Image image;
  Color color;

  @override
  void paint(Canvas canvas, Size size) {
    try{
    canvas.drawImage(image, new Offset(0.0, 0.0), new Paint()..invertColors);
    canvas.drawImage(image, new Offset(0.0, 0.0), new Paint()..colorFilter = ColorFilter.mode(color, BlendMode.color));
    }
    catch(e){
      print(errorHandler(e.toString(),'Paint "None_Style"'));
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }

}