import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PhotoParameters
{
  bool inList;
  String style;
  Image image;
  List parametersList;
  int progressPercent;

  PhotoParameters({this.inList, this.style, this.image, this.parametersList, this.progressPercent});
}
