package com.example.japanizingbeam;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.media.MediaPlayer;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class MyService extends Service {

    MediaPlayer mPlayer;

    @Override
    public void onCreate() {
        super.onCreate();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "messages")
                    .setContentText("Japanizing Beam! is running in background ")
                    .setContentTitle("Flutter background")
                    .setSmallIcon(R.drawable.android_black_24dp);
            startForeground(101, builder.build());
        }

        mPlayer = MediaPlayer.create(this, R.raw.sound);

        if (mPlayer != null) {
            mPlayer.setLooping(true);
            mPlayer.setVolume(5, 5);
            mPlayer.start();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
